﻿using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System;
using System.Threading.Tasks;

namespace Indimin.WebApi.DAO
{
    public class CiudadanoTareaDao
    {
        private Context _context;

        public CiudadanoTareaDao(Context context)
        {
            _context = context;
        }

        public async Task<bool> InsertCiudadanoTarea(CiudadanoTarea oCiudadanoTarea)
        {
            try
            {
                _context.CiudadanoTarea.Add(oCiudadanoTarea);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteCiudadanoTarea(CiudadanoTarea oCiudadanoTarea)
        {
            try
            {
                _context.CiudadanoTarea.Remove(oCiudadanoTarea);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
