﻿using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Indimin.WebApi.DAO
{
    public class TareaDao
    {
        private Context _context;

        public TareaDao(Context context)
        {
            _context = context;
        }

        public async Task<Tarea> GetTareaById(int idTarea)
        {
            return await _context.Tarea.Where(t => t.idTarea == idTarea).FirstOrDefaultAsync();
        }

        public async Task<IList<TareaListDto>> GetTareaByIdCiudadano(int idCiudadano)
        {
            return await (from t in _context.Tarea
                          join tt in _context.TipoTarea on t.idTipoTarea equals tt.idTipoTarea
                          join s in _context.DiaSemana on t.diaSemanaTarea equals s.IdDiaSemana
                          join ct in _context.CiudadanoTarea on t.idTarea equals ct.idTarea
                          where ct.idCiudadano == idCiudadano
                          select new TareaListDto
                          {
                              idTarea = t.idTarea,
                              nombreTarea = t.nombreTarea,
                              descripcionTarea = t.descripcionTarea,
                              idTipoTarea = t.idTipoTarea,
                              nombreTipoTarea = tt.nombreTipoTarea,
                              diaSemanaTarea = t.diaSemanaTarea,
                              nombreDiaSemana = s.nombreDiaSemana,
                              idVigenciaTarea = t.idVigenciaTarea,
                              vigenciaTarea = t.idVigenciaTarea == 1 ? "Vigente" : "No Vigente"
                          }).ToListAsync();
        }

        public async Task<IList<TareaListDto>> GetTareaByFilter(TareaFilterDto oTarea)
        {
            return await (from t in _context.Tarea
                          join tt in _context.TipoTarea on t.idTipoTarea equals tt.idTipoTarea
                          join s in _context.DiaSemana on t.diaSemanaTarea equals s.IdDiaSemana
                          where t.nombreTarea.Contains(oTarea.nombreTarea)
                          && (t.diaSemanaTarea == oTarea.idDiaSemana || oTarea.idDiaSemana == -1)
                          && (t.idTipoTarea == oTarea.idTipoTarea || oTarea.idTipoTarea == -1)
                          select new TareaListDto
                          {
                              idTarea = t.idTarea,
                              nombreTarea = t.nombreTarea,
                              descripcionTarea = t.descripcionTarea,
                              idTipoTarea = t.idTipoTarea,
                              nombreTipoTarea = tt.nombreTipoTarea,
                              diaSemanaTarea = t.diaSemanaTarea,
                              nombreDiaSemana = s.nombreDiaSemana,
                              idVigenciaTarea = t.idVigenciaTarea,
                              vigenciaTarea = t.idVigenciaTarea == 1 ? "Vigente" : "No Vigente"
                          }).ToListAsync();
        }

        public async Task<bool> InsertTarea(Tarea oTarea)
        {
            try
            {
                _context.Tarea.Add(oTarea);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpDateTarea(Tarea oTarea)
        {
            try
            {
                _context.Tarea.Update(oTarea);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
