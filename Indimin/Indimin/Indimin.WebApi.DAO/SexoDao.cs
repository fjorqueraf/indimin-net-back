﻿using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Indimin.WebApi.DAO
{
    public class SexoDao
    {
        private Context _context;

        public SexoDao(Context context)
        {
            _context = context;
        }

        public async Task<IList<Sexo>> GetComboSexo()
        {
            return await _context.Sexo.Where(s => s.idVigenciaSexo == 1).ToListAsync();
        }
    }
}
