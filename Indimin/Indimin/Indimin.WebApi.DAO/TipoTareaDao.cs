﻿using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Indimin.WebApi.DAO
{
    public class TipoTareaDao
    {
        private Context _context;

        public TipoTareaDao(Context context)
        {
            _context = context;
        }

        public async Task<IList<TipoTarea>> GetComboTipoTarea()
        {
            return await _context.TipoTarea.Where(tt => tt.idVigenciaTipoTarea == 1).ToListAsync();
        }
        public async Task<TipoTarea> GetTipoTareaById(int idTipoTarea)
        {
            return await _context.TipoTarea.Where(tt => tt.idTipoTarea == idTipoTarea).FirstOrDefaultAsync();
        }

        public async Task<IList<TipoTareaListDto>> GetTipoTareaByFilter(string nombreTipoTarea)
        {
            return await (from tt in _context.TipoTarea
                          where tt.nombreTipoTarea.Contains(nombreTipoTarea)
                          select new TipoTareaListDto
                          {
                              idTipoTarea = tt.idTipoTarea,
                              nombreTipoTarea = tt.nombreTipoTarea,
                              descripcionTipoTarea = tt.descripcionTipoTarea,
                              idVigenciaTipoTarea = tt.idVigenciaTipoTarea,
                              vigenciaTipoTarea = tt.idVigenciaTipoTarea == 1 ? "Vigente" : "No Vigente"
                          }).ToListAsync();
        }

        public async Task<bool> InsertTipoTarea(TipoTarea oTipoTarea)
        {
            try
            {
                _context.TipoTarea.Add(oTipoTarea);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpDateTipoTarea(TipoTarea oTipoTarea)
        {
            try
            {
                _context.TipoTarea.Update(oTipoTarea);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
