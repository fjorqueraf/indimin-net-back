﻿using System.Threading.Tasks;
using System.Linq;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;

namespace Indimin.WebApi.DAO
{
    public class CiudadanoDao
    {
        private Context _context;

        public CiudadanoDao(Context context)
        {
            _context = context;
        }

        public async Task<Ciudadano> GetCiudadanoById(int idCiudadano)
        {
            return await _context.Ciudadano.Where(c => c.idCiudadano == idCiudadano).FirstOrDefaultAsync();
        }

        public async Task<IList<CiudadanoListDto>> GetCiudadanoByIdTarea(int idTarea)
        {
            return await (from c in _context.Ciudadano
                          join s in _context.Sexo on c.idSexoCiudadano equals s.idSexo
                          join ct in _context.CiudadanoTarea on c.idCiudadano equals ct.idCiudadano
                          where ct.idTarea == idTarea
                          select new CiudadanoListDto
                          {
                              idCiudadano = c.idSexoCiudadano,
                              nombreCiudadano = c.nombreCiudadano,
                              idSexoCiudadano = c.idSexoCiudadano,
                              sexoCiudadano = s.nombreSexo,
                              direccionCiudadano = c.direccionCiudadano,
                              telefonoCiudadano = c.telefonoCiudadano,
                              correoCiudadano = c.correoCiudadano
                          }).ToListAsync();
        }

        public async Task<IList<CiudadanoListDto>> GetCiudadanoByFilter(CiudadanoFilterDto oCiudadano)
        {
            return await (from c in _context.Ciudadano
                          join s in _context.Sexo on c.idSexoCiudadano equals s.idSexo
                          where c.nombreCiudadano.Contains(oCiudadano.nombreCiudadano)
                          && (c.idSexoCiudadano == oCiudadano.idSexoCiudadano || oCiudadano.idSexoCiudadano == -1)
                          select new CiudadanoListDto
                          {
                              idCiudadano = c.idCiudadano,
                              nombreCiudadano = c.nombreCiudadano,
                              idSexoCiudadano = c.idSexoCiudadano,
                              sexoCiudadano = s.nombreSexo,
                              direccionCiudadano = c.direccionCiudadano,
                              telefonoCiudadano = c.telefonoCiudadano,
                              correoCiudadano = c.correoCiudadano
                          }).ToListAsync();
        }

        public async Task<bool> InsertCiudadano(Ciudadano oCiudadano)
        {
            try
            {
                _context.Ciudadano.Add(oCiudadano);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpDateCiudadano(Ciudadano oCiudadano)
        {
            try
            {
                _context.Ciudadano.Update(oCiudadano);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteCiudadano(Ciudadano oCiudadano)
        {
            try
            {
                _context.Ciudadano.Remove(oCiudadano);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
