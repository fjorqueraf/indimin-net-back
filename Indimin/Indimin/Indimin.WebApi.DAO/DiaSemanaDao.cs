﻿using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.DAO
{
    public class DiaSemanaDao
    {
        private Context _context;

        public DiaSemanaDao(Context context)
        {
            _context = context;
        }

        public async Task<IList<DiaSemana>> GetComboDiaSemana()
        {
            return await _context.DiaSemana.ToListAsync();
        }
    }
}
