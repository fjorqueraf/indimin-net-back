﻿namespace Indimin.WebApi.DTO.Filter
{
    public class TareaFilterDto
    {
        public int idDiaSemana { get; set; }
        public string nombreTarea { get; set; }
        public int idTipoTarea { get; set; }
    }
}
