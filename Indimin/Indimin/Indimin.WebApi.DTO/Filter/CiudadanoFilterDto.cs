﻿namespace Indimin.WebApi.DTO.Filter
{
    public class CiudadanoFilterDto
    {
        public string nombreCiudadano { get; set; }
        public int idSexoCiudadano { get; set; }
    }
}
