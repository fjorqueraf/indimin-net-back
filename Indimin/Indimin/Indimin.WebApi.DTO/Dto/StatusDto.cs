﻿namespace Indimin.WebApi.DTO.Dto
{
    public class StatusDto
    {
        public int CodRespuesta { get; set; }
        public string Mensaje { get; set; }
        public dynamic Resp { get; set; }

        public StatusDto(int CodRespuesta, string Mensaje, dynamic Resp)
        {
            this.CodRespuesta = CodRespuesta;
            this.Mensaje = Mensaje;
            this.Resp = Resp;
        }

        public StatusDto()
        {

        }
    }
}
