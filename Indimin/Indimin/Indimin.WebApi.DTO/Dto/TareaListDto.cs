﻿namespace Indimin.WebApi.DTO.Dto
{
    public class TareaListDto
    {
        public int idTarea { get; set; }
        public string nombreTarea { get; set; }
        public string descripcionTarea { get; set; }
        public int idTipoTarea { get; set; }
        public string nombreTipoTarea { get; set; }
        public int diaSemanaTarea { get; set; }
        public string nombreDiaSemana { get; set; }
        public int idVigenciaTarea { get; set; }
        public string vigenciaTarea { get; set; }
    }
}
