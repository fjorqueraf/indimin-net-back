﻿namespace Indimin.WebApi.DTO.Dto
{
    public class TipoTareaListDto
    {
        public int idTipoTarea { get; set; }
        public string nombreTipoTarea { get; set; }
        public string descripcionTipoTarea { get; set; }
        public int idVigenciaTipoTarea { get; set; }
        public string vigenciaTipoTarea { get; set; }
    }
}
