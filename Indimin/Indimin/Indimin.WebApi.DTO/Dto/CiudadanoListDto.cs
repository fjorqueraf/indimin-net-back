﻿namespace Indimin.WebApi.DTO.Dto
{
    public class CiudadanoListDto
    {
        public int idCiudadano { get; set; }
        public string nombreCiudadano { get; set; }
        public int idSexoCiudadano { get; set; }
        public string sexoCiudadano { get; set; }
        public string direccionCiudadano { get; set; }
        public string telefonoCiudadano { get; set; }
        public string correoCiudadano { get; set; }
    }
}
