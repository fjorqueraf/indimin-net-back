﻿using AutoMapper;
using Indimin.WebApi.ENTITY;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Indimin.WebApi.Extension
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                      builder =>
                      {
                          builder.AllowAnyOrigin()
                                 .AllowAnyHeader()
                                 .AllowAnyMethod();
                      });

            });
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

        }

        public static void ConfigureSqlServerContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<Context>(opt => opt.UseSqlServer(config.GetConnectionString("Context")));
        }

        public static void ConfigureSwaggerCore(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
        }

        //public static void ConfigureEmailSettings(this IServiceCollection services, IConfiguration config)
        //{
        //    services.Configure<EmailSettings>(config.GetSection("EmailSettings"));
        //    services.AddTransient<IEmailSender, AuthMessageSender>();
        //}

        public static void UseAutomapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        //public static void ConfigureCronExpresion(this IServiceCollection services)
        //{
        //    //// Add Quartz services
        //    //services.AddSingleton<IJobFactory, SingletonJobFactory>();
        //    //services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

        //    //// Add our job
        //    //services.AddSingleton<SaveOrUpdateUserJobs>();
        //    //services.AddSingleton(new JobSchedule(
        //    //    jobType: typeof(SaveOrUpdateUserJobs),
        //    //    cronExpression: "*/10 * * * * ?")); // run every 1 

        //    //services.AddHostedService<QuartzHostedService>();


        //    services.AddSingleton<ImportUsuariosPortalJob>();
        //    services.AddHostedService<ImportUsuariosPortalJob>();

        //    services.AddSingleton<VencidosMensajeriaJob>();
        //    services.AddHostedService<VencidosMensajeriaJob>();

        //    services.AddSingleton<MensajeriaJob>();
        //    services.AddHostedService<MensajeriaJob>();
        //}
    }
}
