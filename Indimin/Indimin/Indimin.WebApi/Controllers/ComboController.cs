﻿using System.Threading.Tasks;
using Indimin.WebApi.BCP.Impl;
using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using Microsoft.AspNetCore.Mvc;

namespace Indimin.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ComboController : ControllerBase
    {
        private readonly Context _context;
        private readonly IComboSvc iComboSvc;

        public ComboController(Context context)
        {
            _context = context;
            iComboSvc = new ComboSvcImpl(_context);
        }

        [HttpGet]
        public async Task<ActionResult<StatusDto>> GetComboDiaSemana()
        {
            var oStatus = await Task.Run(() => iComboSvc.GetComboDiaSemana());
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }

        [HttpGet]
        public async Task<ActionResult<StatusDto>> GetComboSexo()
        {
            var oStatus = await Task.Run(() => iComboSvc.GetComboSexo());
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }

        [HttpGet]
        public async Task<ActionResult<StatusDto>> GetComboTipoTarea()
        {
            var oStatus = await Task.Run(() => iComboSvc.GetComboTipoTarea());
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }
    }
}
