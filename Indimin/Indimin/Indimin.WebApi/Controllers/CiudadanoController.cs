﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Indimin.WebApi.BCP.Impl;
using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.AspNetCore.Mvc;

namespace Indimin.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CiudadanoController : ControllerBase
    {
        private readonly Context _context;
        private readonly ICiudadanoSvc iCiudadanoSvc;

        public CiudadanoController(Context context)
        {
            _context = context;
            iCiudadanoSvc = new CiudadanoSvcImpl(_context);
        }

        [HttpGet("{idCiudadano}")]
        public async Task<ActionResult<Ciudadano>> getCiudadanoById(int idCiudadano)
        {
            return await Task.Run(() => iCiudadanoSvc.getCiudadanoById(idCiudadano));
        }

        [HttpGet("{idTarea}")]
        public async Task<IList<CiudadanoListDto>> GetCiudadanoByIdTarea(int idTarea)
        {
            return await Task.Run(() => iCiudadanoSvc.GetCiudadanoByIdTarea(idTarea));
        }

        [HttpPost]
        public async Task<IList<CiudadanoListDto>> GetCiudadanoByFilter([FromBody] CiudadanoFilterDto oCiudadano)
        {
            return await Task.Run(() => iCiudadanoSvc.GetCiudadanoByFilter(oCiudadano));
        }

        [HttpPost]
        public async Task<ActionResult<StatusDto>> MaintainerCiudadano([FromBody] Ciudadano oCiudadano)
        {
            var oStatus = await Task.Run(() => iCiudadanoSvc.MaintainerCiudadano(oCiudadano));
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }

        [HttpGet("{idCiudadano}")]
        public async Task<ActionResult<StatusDto>> DeleteCiudadano(int idCiudadano)
        {
            var oStatus = await Task.Run(() => iCiudadanoSvc.DeleteCiudadano(idCiudadano));
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }
    }
}
