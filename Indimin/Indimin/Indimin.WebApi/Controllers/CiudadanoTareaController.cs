﻿using System.Threading.Tasks;
using Indimin.WebApi.BCP.Impl;
using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.AspNetCore.Mvc;

namespace Indimin.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CiudadanoTareaController : ControllerBase
    {
        private readonly Context _context;
        private readonly ICiudadanoTareaSvc iciudadanoTareaSvc;

        public CiudadanoTareaController(Context context)
        {
            _context = context;
            iciudadanoTareaSvc = new CiudadanoTareaSvcImpl(_context);
        }

        [HttpPost]
        public async Task<ActionResult<StatusDto>> InsertCiudadanoTarea([FromBody] CiudadanoTarea oCiudadanoTarea)
        {
            var oStatus = await Task.Run(() => iciudadanoTareaSvc.InsertCiudadanoTarea(oCiudadanoTarea));
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }

        [HttpPost]
        public async Task<ActionResult<StatusDto>> DeleteCiudadanoTarea([FromBody] CiudadanoTarea oCiudadanoTarea)
        {
            var oStatus = await Task.Run(() => iciudadanoTareaSvc.DeleteCiudadanoTarea(oCiudadanoTarea));
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }
    }
}
