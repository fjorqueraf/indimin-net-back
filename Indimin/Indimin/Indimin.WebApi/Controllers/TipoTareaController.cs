﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Indimin.WebApi.BCP.Impl;
using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.AspNetCore.Mvc;

namespace Indimin.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TipoTareaController : ControllerBase
    {
        private readonly Context _context;
        private readonly ITipoTareaSvc iTipoTareaSvc;
        public TipoTareaController(Context context)
        {
            _context = context;
            iTipoTareaSvc = new TipoTareaSvcImpl(_context);
        }

        [HttpGet("{idTipoTarea}")]
        public async Task<ActionResult<TipoTarea>> GetTipoTareaByFilter(int idTipoTarea)
        {
            return await Task.Run(() => iTipoTareaSvc.GetTipoTareaById(idTipoTarea));
        }

        [HttpGet("{nombreTipoTarea}")]
        public async Task<IList<TipoTareaListDto>> GetTipoTareaByFilter(string nombreTipoTarea)
        {
            return await Task.Run(() => iTipoTareaSvc.GetTipoTareaByFilter(nombreTipoTarea));
        }

        [HttpPost]
        public async Task<ActionResult<StatusDto>> MaintainerTipoTarea([FromBody] TipoTarea oTipoTarea)
        {
            return await Task.Run(() => iTipoTareaSvc.MaintainerTipoTarea(oTipoTarea));
        }

        [HttpGet("{idTipoTarea}")]
        public async Task<ActionResult<StatusDto>> DeleteTipoTarea(int idTipoTarea)
        {
            return await Task.Run(() => iTipoTareaSvc.DeleteTipoTarea(idTipoTarea));
        }
    }
}
