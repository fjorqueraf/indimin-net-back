﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Indimin.WebApi.BCP.Impl;
using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.AspNetCore.Mvc;

namespace Indimin.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TareaController : ControllerBase
    {
        private readonly Context _context;
        private readonly ITareaSvc iTareaSvc;
        public TareaController(Context context)
        {
            _context = context;
            iTareaSvc = new TareaSvcImpl(_context);
        }

        [HttpGet("{idTarea}")]
        public async Task<ActionResult<Tarea>> GetTareaById(int idTarea)
        {
            return await Task.Run(() => iTareaSvc.GetTareaById(idTarea));
        }

        [HttpGet("{idCiudadano}")]
        public async Task<IList<TareaListDto>> GetTareaByIdCiudadano(int idCiudadano)
        {
            return await Task.Run(() => iTareaSvc.GetTareaByIdCiudadano(idCiudadano));
        }

        [HttpPost]
        public async Task<IList<TareaListDto>> GetTareaByFilter([FromBody] TareaFilterDto oTarea)
        {
            return await Task.Run(() => iTareaSvc.GetTareaByFilter(oTarea));
        }

        [HttpPost]
        public async Task<ActionResult<StatusDto>> MaintainerTarea([FromBody] Tarea oTarea)
        {
            var oStatus = await Task.Run(() => iTareaSvc.MaintainerTarea(oTarea));
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }

        [HttpGet("{idTarea}")]
        public async Task<ActionResult<StatusDto>> DeleteTarea(int idTarea)
        {
            var oStatus = await Task.Run(() => iTareaSvc.DeleteTarea(idTarea));
            if (oStatus.CodRespuesta == 200)
            {
                return Ok(oStatus);
            }
            else
            {
                return BadRequest(oStatus);
            }
        }
    }
}
