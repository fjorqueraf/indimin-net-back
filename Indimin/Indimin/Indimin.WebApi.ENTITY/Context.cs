﻿using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using Microsoft.EntityFrameworkCore;

namespace Indimin.WebApi.ENTITY
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }
        #region <--Tablas-->
        public virtual DbSet<Ciudadano> Ciudadano { get; set; }
        public virtual DbSet<CiudadanoTarea> CiudadanoTarea { get; set; }
        public virtual DbSet<DiaSemana> DiaSemana { get; set; }
        public virtual DbSet<Sexo> Sexo { get; set; }        
        public virtual DbSet<Tarea> Tarea { get; set; }
        public virtual DbSet<TipoTarea> TipoTarea { get; set; }
        #endregion
    }
}
