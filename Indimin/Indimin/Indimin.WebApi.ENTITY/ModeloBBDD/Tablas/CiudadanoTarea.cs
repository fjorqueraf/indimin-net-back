﻿using System.ComponentModel.DataAnnotations;

namespace Indimin.WebApi.ENTITY.ModeloBBDD.Tablas
{
    public class CiudadanoTarea
    {
        [Key]
        public int idCiudadano { get; set; }
        public int idTarea { get; set; }
    }
}
