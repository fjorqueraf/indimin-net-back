﻿using System.ComponentModel.DataAnnotations;

namespace Indimin.WebApi.ENTITY.ModeloBBDD.Tablas
{
    public class Tarea
    {
        [Key]
        public int idTarea { get; set; }
        public string nombreTarea { get; set; }
        public string descripcionTarea { get; set; }
        public int idTipoTarea { get; set; }
        public int diaSemanaTarea { get; set; }
        public int idVigenciaTarea { get; set; }
    }
}
