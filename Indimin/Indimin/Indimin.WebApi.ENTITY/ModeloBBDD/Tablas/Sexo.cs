﻿using System.ComponentModel.DataAnnotations;

namespace Indimin.WebApi.ENTITY.ModeloBBDD.Tablas
{
    public class Sexo
    {
        [Key]
        public int idSexo { get; set; }
        public string nombreSexo { get; set; }
        public string DescripcionSexo { get; set; }
        public int idVigenciaSexo { get; set; }
    }
}
