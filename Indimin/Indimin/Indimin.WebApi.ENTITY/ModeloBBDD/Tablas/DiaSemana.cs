﻿using System.ComponentModel.DataAnnotations;

namespace Indimin.WebApi.ENTITY.ModeloBBDD.Tablas
{
    public class DiaSemana
    {
        [Key]
        public int IdDiaSemana { get; set; }
        public int numeroDiaSemana { get; set; }
        public string nombreDiaSemana { get; set; }
    }
}
