﻿using System.ComponentModel.DataAnnotations;

namespace Indimin.WebApi.ENTITY.ModeloBBDD.Tablas
{
    public class Ciudadano
    {
        [Key]
        public int idCiudadano { get; set; }
        public string nombreCiudadano { get; set; }
        public int idSexoCiudadano { get; set; }
        public string direccionCiudadano { get; set; }
        public string telefonoCiudadano { get; set; }
        public string correoCiudadano { get; set; }
    }
}
