﻿using System.ComponentModel.DataAnnotations;

namespace Indimin.WebApi.ENTITY.ModeloBBDD.Tablas
{
    public class TipoTarea
    {
        [Key]
        public int idTipoTarea { get; set; }
        public string nombreTipoTarea { get; set; }
        public string descripcionTipoTarea { get; set; }
        public int idVigenciaTipoTarea { get; set; }
    }
}
