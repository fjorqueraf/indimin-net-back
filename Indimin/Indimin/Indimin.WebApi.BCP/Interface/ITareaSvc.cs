﻿using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Interface
{
    public interface ITareaSvc
    {
        Task<Tarea> GetTareaById(int idTarea);
        Task<IList<TareaListDto>> GetTareaByIdCiudadano(int idCiudadano);
        Task<IList<TareaListDto>> GetTareaByFilter(TareaFilterDto oTarea);
        Task<StatusDto> MaintainerTarea(Tarea oTarea);
        Task<StatusDto> DeleteTarea(int idTarea);
    }
}
