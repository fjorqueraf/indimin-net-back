﻿using Indimin.WebApi.DTO.Dto;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Interface
{
    public interface IComboSvc
    {
        Task<StatusDto> GetComboDiaSemana();
        Task<StatusDto> GetComboSexo();
        Task<StatusDto> GetComboTipoTarea();
    }
}
