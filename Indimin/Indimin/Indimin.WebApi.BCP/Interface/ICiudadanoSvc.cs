﻿using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Interface
{
    public interface ICiudadanoSvc
    {
        Task<Ciudadano> getCiudadanoById(int idCiudadano);
        Task<IList<CiudadanoListDto>> GetCiudadanoByIdTarea(int idTarea);
        Task<IList<CiudadanoListDto>> GetCiudadanoByFilter(CiudadanoFilterDto oCiudadano);
        Task<StatusDto> MaintainerCiudadano(Ciudadano oCiudadano);
        Task<StatusDto> DeleteCiudadano(int idCiudadano);
    }
}
