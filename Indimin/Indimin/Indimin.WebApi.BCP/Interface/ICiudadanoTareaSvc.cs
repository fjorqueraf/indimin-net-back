﻿using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Interface
{
    public interface ICiudadanoTareaSvc
    {
        Task<StatusDto> InsertCiudadanoTarea(CiudadanoTarea oCiudadanoTarea);
        Task<StatusDto> DeleteCiudadanoTarea(CiudadanoTarea oCiudadanoTarea);
    }
}
