﻿using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Interface
{
    public interface ITipoTareaSvc
    {
        Task<TipoTarea> GetTipoTareaById(int idTipoTarea);
        Task<IList<TipoTareaListDto>> GetTipoTareaByFilter(string nombreTipoTarea);
        Task<StatusDto> MaintainerTipoTarea(TipoTarea oTipoTarea);
        Task<StatusDto> DeleteTipoTarea(int idTipoTarea);

    }
}
