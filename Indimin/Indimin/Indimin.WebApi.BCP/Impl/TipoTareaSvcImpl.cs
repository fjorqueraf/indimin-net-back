﻿using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DAO;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Impl
{
    public class TipoTareaSvcImpl : ITipoTareaSvc
    {
        private readonly Context _context;
        private readonly TipoTareaDao tipoTareaDao;
        public TipoTareaSvcImpl(Context context)
        {
            _context = context;
            tipoTareaDao = new TipoTareaDao(_context);
        }

        public async Task<TipoTarea> GetTipoTareaById(int idTipoTarea)
        {
            return await tipoTareaDao.GetTipoTareaById(idTipoTarea);
        }

        public async Task<IList<TipoTareaListDto>> GetTipoTareaByFilter(string nombreTipoTarea)
        {
            return await tipoTareaDao.GetTipoTareaByFilter(nombreTipoTarea);
        }

        public async Task<StatusDto> MaintainerTipoTarea(TipoTarea oTipoTarea)
        {
            try
            {
                bool resp = false;
                if (oTipoTarea.idTipoTarea == 0)
                {
                    resp = await tipoTareaDao.InsertTipoTarea(oTipoTarea);
                }
                else
                {
                    resp = await tipoTareaDao.UpDateTipoTarea(oTipoTarea);
                }
                return new StatusDto(200, "Tipo Tarea guardado exitosamente", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }

        public async Task<StatusDto> DeleteTipoTarea(int idTipoTarea)
        {
            try
            {
                var oTipoTarea = await tipoTareaDao.GetTipoTareaById(idTipoTarea);
                oTipoTarea.idVigenciaTipoTarea = 2;
                var resp = tipoTareaDao.UpDateTipoTarea(oTipoTarea);
                return new StatusDto(200, "Se ha dejado no vigente el tipo tarea", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }
    }
}
