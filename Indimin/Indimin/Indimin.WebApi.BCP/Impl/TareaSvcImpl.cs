﻿using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DAO;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Impl
{
    public class TareaSvcImpl: ITareaSvc
    {
        private readonly Context _context;
        private readonly TareaDao tareaDao;

        public TareaSvcImpl(Context context)
        {
            _context = context;
            tareaDao = new TareaDao(_context);
        }

        public async Task<Tarea> GetTareaById(int idTarea)
        {
            return await tareaDao.GetTareaById(idTarea);
        }

        public async Task<IList<TareaListDto>> GetTareaByIdCiudadano(int idCiudadano)
        {
            return await tareaDao.GetTareaByIdCiudadano(idCiudadano);
        }

        public async Task<IList<TareaListDto>> GetTareaByFilter(TareaFilterDto oTarea)
        {
            return await tareaDao.GetTareaByFilter(oTarea);
        }

        public async Task<StatusDto> MaintainerTarea(Tarea oTarea)
        {
            try
            {
                bool resp = false;
                if (oTarea.idTarea== 0)
                {
                    resp = await tareaDao.InsertTarea(oTarea);
                }
                else
                {
                    resp = await tareaDao.UpDateTarea(oTarea);
                }
                return new StatusDto(200, "Tarea guardada exitosamente", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }

        public async Task<StatusDto> DeleteTarea(int idTarea)
        {
            try
            {
                var oTarea = await tareaDao.GetTareaById(idTarea);
                oTarea.idVigenciaTarea = 2;
                var resp = tareaDao.UpDateTarea(oTarea);
                return new StatusDto(200, "Se ha dejado no vigente la tarea seleccionada", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }
    }
}
