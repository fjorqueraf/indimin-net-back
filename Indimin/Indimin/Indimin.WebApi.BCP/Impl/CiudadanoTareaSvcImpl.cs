﻿using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DAO;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Impl
{
    public class CiudadanoTareaSvcImpl : ICiudadanoTareaSvc
    {
        private readonly Context _context;
        private readonly CiudadanoTareaDao ciudadanoTareaDao;

        public CiudadanoTareaSvcImpl(Context context)
        {
            _context = context;
            ciudadanoTareaDao = new CiudadanoTareaDao(_context);
        }

        public async Task<StatusDto> InsertCiudadanoTarea(CiudadanoTarea oCiudadanoTarea)
        {
            try
            {
                var resp = await ciudadanoTareaDao.InsertCiudadanoTarea(oCiudadanoTarea);
                return new StatusDto(200, "Se ha guardado correctmante la asociacion ciudadano - tarea", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }

        public async Task<StatusDto> DeleteCiudadanoTarea(CiudadanoTarea oCiudadanoTarea)
        {
            try
            {
                var resp = await ciudadanoTareaDao.DeleteCiudadanoTarea(oCiudadanoTarea);
                return new StatusDto(200, "Se ha eliminado correctmante la asociacion ciudadano - tarea", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }
    }
}
