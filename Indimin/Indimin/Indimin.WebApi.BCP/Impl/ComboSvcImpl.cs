﻿using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DAO;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.ENTITY;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Impl
{
    public class ComboSvcImpl : IComboSvc
    {
        private readonly Context _context;
        private readonly DiaSemanaDao diaSemanaDao;
        private readonly SexoDao sexoDao;
        private readonly TipoTareaDao tipoTareaDao;
        public ComboSvcImpl(Context context)
        {
            _context = context;
            diaSemanaDao = new DiaSemanaDao(_context);
            sexoDao = new SexoDao(_context);
            tipoTareaDao = new TipoTareaDao(_context);
        }

        public async Task<StatusDto> GetComboDiaSemana()
        {
            var resp = await diaSemanaDao.GetComboDiaSemana();
            if (resp != null || resp.Count > 0)
            {
                return new StatusDto(200, "OK", resp);
            }
            else
            {
                return new StatusDto(400, "No se han encontrado datos del combo Dia Semana", false);
            }
        }

        public async Task<StatusDto> GetComboSexo()
        {
            var resp = await sexoDao.GetComboSexo();
            if (resp != null || resp.Count > 0)
            {
                return new StatusDto(200, "OK", resp);
            }
            else
            {
                return new StatusDto(400, "No se han encontrado datos del combo Sexo", false);
            }
        }

        public async Task<StatusDto> GetComboTipoTarea()
        {
            var resp = await tipoTareaDao.GetComboTipoTarea();
            if (resp != null || resp.Count > 0)
            {
                return new StatusDto(200, "OK", resp);
            }
            else
            {
                return new StatusDto(400, "No se han encontrado datos del combo Tipo Tarea", false);
            }
        }

    }
}
