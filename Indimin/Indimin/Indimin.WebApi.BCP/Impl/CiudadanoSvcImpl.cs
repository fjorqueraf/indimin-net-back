﻿using Indimin.WebApi.BCP.Interface;
using Indimin.WebApi.DAO;
using Indimin.WebApi.DTO.Dto;
using Indimin.WebApi.DTO.Filter;
using Indimin.WebApi.ENTITY;
using Indimin.WebApi.ENTITY.ModeloBBDD.Tablas;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Indimin.WebApi.BCP.Impl
{
    public class CiudadanoSvcImpl : ICiudadanoSvc
    {
        private readonly Context _context;
        private readonly CiudadanoDao ciudadanoDao;
        public CiudadanoSvcImpl(Context context)
        {
            _context = context;
            ciudadanoDao = new CiudadanoDao(_context);
        }

        public async Task<Ciudadano> getCiudadanoById(int idCiudadano)
        {
            return await ciudadanoDao.GetCiudadanoById(idCiudadano);
        }

        public async Task<IList<CiudadanoListDto>> GetCiudadanoByIdTarea(int idTarea)
        {
            return await ciudadanoDao.GetCiudadanoByIdTarea(idTarea);
        }

        public async Task<IList<CiudadanoListDto>> GetCiudadanoByFilter(CiudadanoFilterDto oCiudadano)
        {
            return await ciudadanoDao.GetCiudadanoByFilter(oCiudadano);
        }

        public async Task<StatusDto> MaintainerCiudadano(Ciudadano oCiudadano)
        {
            try
            {
                bool resp = false;
                if (oCiudadano.idCiudadano == 0)
                {
                    resp = await ciudadanoDao.InsertCiudadano(oCiudadano);
                }
                else
                {
                    resp = await ciudadanoDao.UpDateCiudadano(oCiudadano);
                }
                return new StatusDto(200, "Ciudadano guardado exitosamente", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }

        public async Task<StatusDto> DeleteCiudadano(int idCiudadano)
        {
            try
            {
                var oCiudadano = await ciudadanoDao.GetCiudadanoById(idCiudadano);
                var resp = ciudadanoDao.DeleteCiudadano(oCiudadano);
                return new StatusDto(200, "Se ha eliminado el ciudadano", resp);
            }
            catch (Exception ex)
            {
                return new StatusDto(400, "Problemas internos : " + ex.Message, false);
            }
        }
    }
}
